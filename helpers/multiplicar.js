const fs = require('fs');
const colors = require('colors');

const crearArchivo = async(base=5,listar=true,hasta=10) => {
  try {
    let salida = '';
    let consola = '';
    
    for(let multiplicador=1;multiplicador<=hasta;multiplicador++) {
      salida +=`${base} x ${multiplicador} = ${base*multiplicador}\n`;
      consola +=`${base} ${'x'.green} ${multiplicador} ${'='.green} ${base*multiplicador}\n`;
    }
    
    if(listar) {
      console.log(`==================`.green);
      console.log(`Tabla del ${colors.blue(base)}`);
      console.log(`==================`.green);
      console.log(consola);
    }
  
    fs.writeFileSync(`./salida/tabla-${base}.txt`,salida);
  
    return `tabla-${base}.txt`;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  crearArchivo
}